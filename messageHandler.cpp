#include "fileDownloader.hpp"
#include "messageHandler.hpp"
#include <condition_variable>
#include <mutex>
#include <string>
#include <thread>


MessageHandler::MessageHandler():hasNewMessage{false},cancelThread{false},msgMutex{std::mutex()},messageHandlerCounter{0},fileDownloader{FileDownloader()},messageReader{MessageReader()},display{Display()}{
    currentMsg = messageReader.readMsg(pathToMsgFile);
    display.displayMessage(currentMsg.message);
    checkForMessagesThread = std::thread([this]{this->checkForMessages(&hasNewMessage);});
}

MessageHandler::~MessageHandler(){
    if(checkForMessagesThread.joinable()){
        cancelThread = true;
        checkForMessagesThread.join();
    }
}

void MessageHandler::sendToDisplay(){
    std::scoped_lock lock(msgMutex);
    if(hasNewMessage == true){
        display.displayMessage(currentMsg.message);
        hasNewMessage = false;
    }
}

void MessageHandler::checkForMessages(bool* hasNewMsg){
    for(;;){
        if(cancelThread == true){
            return;
        }
        if(messageHandlerCounter == 30){
            if(fileDownloader.downloadFile() == 0){
                *hasNewMsg = isDownloadedMesageNew();
            }
            messageHandlerCounter = 0;
        }else{
            messageHandlerCounter++;
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }
    }
}

bool MessageHandler::isDownloadedMesageNew(){
    std::scoped_lock lock(msgMutex);
    Message newMsg = messageReader.readMsg(pathToMsgFile);
    if(newMsg.id != currentMsg.id){
        currentMsg = newMsg;
        return true;
    }
    return false;
}