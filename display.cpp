#include "DEV_Config.h"
#include "display.hpp"
#include "EPD_2in9b_V3.h"
#include "GUI_Paint.h"
#include "message.hpp"

#include <boost/tokenizer.hpp>
#include <cstdlib> 
#include <iostream>
#include <math.h>
#include <string>
#include <vector>

Display::Display(){
    DEV_Module_Init();
}

Display::~Display(){
    DEV_Module_Exit();
}

void Display::displayMessage(std::variant<TextMessage,ImageMessage>& message){
    EPD_2IN9B_V3_Init();

    UBYTE *BlackImage, *RedImage;
    constexpr UWORD Imagesize = ((EPD_2IN9B_V3_WIDTH % 8 == 0)? (EPD_2IN9B_V3_WIDTH / 8 ): (EPD_2IN9B_V3_WIDTH / 8 + 1)) * EPD_2IN9B_V3_HEIGHT;

    if((BlackImage = (UBYTE *) std::malloc(Imagesize)) == NULL || (RedImage = (UBYTE *)std::malloc(Imagesize)) == NULL ) {
        return;
    }

    Paint_NewImage(BlackImage, EPD_2IN9B_V3_WIDTH, EPD_2IN9B_V3_HEIGHT, 270, WHITE);
    Paint_NewImage(RedImage, EPD_2IN9B_V3_WIDTH, EPD_2IN9B_V3_HEIGHT, 270, WHITE);
    Paint_SelectImage(BlackImage);
    Paint_Clear(WHITE);

    std::visit(
        Overload{
            [this](TextMessage& text){this->displayText(text.content);},
            [this](ImageMessage& image){this->displayImage(image.path);}
        }, message
    );

    Paint_SelectImage(RedImage);
    Paint_Clear(WHITE);
    EPD_2IN9B_V3_Display(BlackImage, RedImage);
    EPD_2IN9B_V3_Sleep();
    std::free(BlackImage);
    std::free(RedImage);

    return;
}

void Display::displayText(std::string& msg){
    std::vector<std::string> lines = getLines(msg);
    int startYPos = getYStartPosition(lines.size());
    for(int i = 0; i<lines.size();i++){
        std::cout << lines[i] <<"\n";
        Paint_DrawString_EN(getXPosition(lines[i]),startYPos + (i*18),lines[i].c_str(),&Font20,WHITE,BLACK);
    }
}

void Display::displayImage(std::string path){
    throw "Nicht Implementiert";
}

std::vector<std::string> Display::getLines(std::string& msg){
    typedef boost::tokenizer<boost::char_separator<char>> tokenizer;
    boost::char_separator<char> sep{" "};
    tokenizer tok{msg, sep};  
    
    std::string currentLine;
    int maxCharsPerLine = 21;
    std::vector<std::string> lines;

    for(auto beg = tok.begin();beg != tok.end(); ++beg){
        if((currentLine.size() + beg->size()+1) < maxCharsPerLine){
            currentLine += *beg + " ";       
        }else{
            lines.push_back(currentLine);
            currentLine = *beg + " ";
        }
    }
    lines.push_back(currentLine);

    return lines;
}

int Display::getXPosition(std::string& line){
    int xPos = (EPD_2IN9B_V3_HEIGHT - (static_cast<int>(line.length()) * 14)) / 2;
    return xPos >= 0 ? xPos : 0;
}

int Display::getYStartPosition(int amountLinesNeeded){
    return (EPD_2IN9B_V3_WIDTH / 2) - 10 - ((amountLinesNeeded - 1)*10);
}