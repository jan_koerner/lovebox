#include "message.hpp"
#include "messageReader.hpp"
#include <filesystem>
#include <fstream>
#include <string>

Message MessageReader::readMsg(std::string path){
    Message msg;
    TextMessage text;
    if(std::filesystem::exists(path)){
        std::ifstream msgFile;
        msgFile.open(path);
        if(msgFile.is_open()){
            int currentLine = 0;
            std::string lineContent;
            while(currentLine < 2 && std::getline(msgFile, lineContent)){
                if(currentLine == 0){
                    msg.id = std::stoi(lineContent);
                }else if(currentLine == 1){
                    text.content = lineContent;
                }
                currentLine++;
            }
        }
        msgFile.close();
    }else{
        msg.id = 0;
        text.content = "Bisher keine Nachrichten empfangen";
    }
    
    msg.message = text;
    return msg;
}
