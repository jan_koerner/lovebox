#include "fileDownloader.hpp"
#include <cstdlib>
#include <string>

int FileDownloader::downloadFile(){
    std::string cmd = "scp -q -P " + std::to_string(port) + " -i " + keyFile + " " + user + "@" + remotePath + " " + localPath;
    return std::system(cmd.c_str());
}