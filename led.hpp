#ifndef LED_HPP
#define LED_HPP

class Led{
    public:
        Led(unsigned int gpioPort);
        ~Led(void);
        void turnOn(void);
        void turnOff(void);
    private:
        unsigned int gpio;
        bool isTurnedOn;
};

#endif