#include "motor.hpp"
#include <chrono>
#include <pigpio.h>
#include <thread>


Motor::Motor(unsigned a, unsigned b, unsigned c, unsigned d) : a{a},b{b},c{c},d{d},steps{{{0,0,0,1},{0,0,1,1},{0,0,1,0},{0,1,1,0},{0,1,0,0},{1,1,0,0},{1,0,0,0},{1,0,0,1}}},cancelThread{false}{
    gpioSetMode(a,PI_OUTPUT);
    gpioSetMode(b,PI_OUTPUT);
    gpioSetMode(c,PI_OUTPUT);
    gpioSetMode(d,PI_OUTPUT);
    gpioWrite(a,0);
    gpioWrite(b,0);
    gpioWrite(c,0);
    gpioWrite(d,0);
}

Motor::~Motor(){
    stop();
}

void Motor::stop(){
    gpioWrite(a,0);
    gpioWrite(b,0);
    gpioWrite(c,0);
    gpioWrite(d,0);  
    if(moveThread.joinable()){
        cancelThread = true;
        moveThread.join();
    }
}

void Motor::start(){
    moveThread = std::thread([this]{this->move();});
}

void Motor::move(){
    for(;;){
        if(!cancelThread){
            rotateCounterClockWise(180);
            std::this_thread::sleep_for(std::chrono::milliseconds(25));
            rotateClockWise(25);
            std::this_thread::sleep_for(std::chrono::milliseconds(25));
        }else{
            cancelThread=false;
            return;
        }
    }
}

void Motor::rotateClockWise(unsigned degree){
    unsigned amountSteps = getAmountSteps(degree);
    for(unsigned i=0;i<amountSteps;i++){
        for(unsigned i=0;i<steps.size();i++){
            if(!cancelThread){
                singleStep(steps[i]);
            }else{
                return;
            }
        }  
    } 
}

void Motor::rotateCounterClockWise(unsigned degree){
    unsigned amountSteps = getAmountSteps(degree);
    for(unsigned i=0;i<amountSteps;i++){
        for(unsigned i=steps.size();i>0;i--){
            if(!cancelThread){
                singleStep(steps[i-1]);
            }else{
                return;
            }
        }  
    }
}

void Motor::singleStep(std::array<unsigned,4> stepArr){
            gpioWrite(a,stepArr[0]);
            gpioWrite(b,stepArr[1]);
            gpioWrite(c,stepArr[2]);
            gpioWrite(d,stepArr[3]);
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
}

unsigned getAmountSteps(unsigned degree){
    return static_cast<int>((degree/360.0)*512);
}
