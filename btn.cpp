#include "btn.hpp"
#include "notificationHandler.hpp"
#include <pigpio.h>

Btn::Btn(unsigned int gpioPort):changedState{true},currentLevel{1},gpio{gpioPort}{
    gpioSetMode(gpio,PI_INPUT);
}

int Btn::read(void){
    return gpioRead(gpio);
}
