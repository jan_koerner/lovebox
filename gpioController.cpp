#include "gpioController.hpp"
#include <pigpio.h>

GpioController::GpioController(){
    isInitialized = (gpioInitialise()>=0);
}

GpioController::~GpioController(){
    gpioTerminate();
}