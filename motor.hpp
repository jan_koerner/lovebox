#ifndef MOTOR_HPP
#define MOTOR_HPP
#include <array>
#include <atomic>
#include <thread>

class Motor{
    public:
        Motor(unsigned a, unsigned b, unsigned c, unsigned d);
        ~Motor(void);
        void stop(void);
        void start(void);

    private:
        unsigned a;
        unsigned b;
        unsigned c;
        unsigned d;
        
        std::array<std::array<unsigned,4>,8> steps;
        std::thread moveThread;
        std::atomic<bool> cancelThread;

        void move(void);
        void rotateClockWise(unsigned degree);
        void rotateCounterClockWise(unsigned degree);
        void singleStep(std::array<unsigned,4> singleStep);
};

unsigned getAmountSteps(unsigned degree);

#endif