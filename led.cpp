#include "led.hpp"
#include <pigpio.h>

Led::Led(unsigned int gpioPort):gpio{gpioPort}{
    gpioSetMode(gpio,PI_OUTPUT);
    gpioWrite(gpio,0);
}

Led::~Led(void){
    turnOff();
}

void Led::turnOn(void){
    gpioWrite(gpio,1);
}

void Led::turnOff(void){
    gpioWrite(gpio,0);
}