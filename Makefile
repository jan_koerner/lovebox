CXX=g++
CXXFLAGS=-Wall -g -Wextra -pedantic -std=c++17
OBJS = main.o btn.o led.o gpioController.o notificationHandler.o motor.o messageHandler.o fileDownloader.o messageReader.o display.o
OBJS_EPAPER = ./e-Paper/RaspberryPi_JetsonNano/c/bin/EPD_2in9b_V3.o ./e-Paper/RaspberryPi_JetsonNano/c/bin/GUI_BMPfile.o ./e-Paper/RaspberryPi_JetsonNano/c/bin/GUI_Paint.o ./e-Paper/RaspberryPi_JetsonNano/c/bin/font12.o ./e-Paper/RaspberryPi_JetsonNano/c/bin/font16.o ./e-Paper/RaspberryPi_JetsonNano/c/bin/font20.o ./e-Paper/RaspberryPi_JetsonNano/c/bin/font8.o ./e-Paper/RaspberryPi_JetsonNano/c/bin/font24.o ./e-Paper/RaspberryPi_JetsonNano/c/bin/dev_hardware_SPI.o ./e-Paper/RaspberryPi_JetsonNano/c/bin/RPI_sysfs_gpio.o ./e-Paper/RaspberryPi_JetsonNano/c/bin/DEV_Config.o
LDLIBS = -lpigpio -lm -lpthread -lstdc++fs
CC = gcc

.Phony:all
all: lovebox

lovebox: $(OBJS)               
	$(CXX) $(CXXFLAGS) $(OBJS) ${OBJS_EPAPER} $(LDLIBS) -I./e-Paper/RaspberryPi_JetsonNano/c/lib/Config -I./e-Paper/RaspberryPi_JetsonNano/c/lib/e-Paper -I./e-Paper/RaspberryPi_JetsonNano/c/lib/Fonts -I./e-Paper/RaspberryPi_JetsonNano/c/lib/GUI -o lovebox.elf 

main.o: main.cpp btn.hpp gpioController.hpp messageHandler.hpp notificationHandler.hpp      
btn.o: btn.cpp btn.hpp notificationHandler.hpp               
led.o: led.cpp led.hpp
gpioController.o: gpioController.cpp gpioController.hpp
notificationHandler.o: notificationHandler.cpp notificationHandler.hpp led.hpp motor.hpp
motor.o: motor.hpp motor.cpp
messageHandler.o: messageHandler.hpp messageHandler.cpp fileDownloader.hpp messageReader.hpp display.hpp
fileDownloader.o: fileDownloader.hpp fileDownloader.cpp
messageReader.o: messageReader.hpp messageReader.cpp 
display.o: display.cpp display.hpp
	${CC} ${CXXFLAGS} -I./e-Paper/RaspberryPi_JetsonNano/c/lib/Config -I./e-Paper/RaspberryPi_JetsonNano/c/lib/e-Paper -I./e-Paper/RaspberryPi_JetsonNano/c/lib/Fonts -I./e-Paper/RaspberryPi_JetsonNano/c/lib/GUI -c ${LDLIBS} -o display.o display.cpp 

.Phony:clean
clean: 
	rm -f *~ *.o lovebox.elf

