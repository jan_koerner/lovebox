#ifndef MESSAGE_READER_HPP
#define MESSAGE_READER_HPP
#include <string>

struct Message;

class MessageReader{
    public:
        Message readMsg(std::string path);
};

#endif