#ifndef FILE_DOWNLOADER_HPP
#define FILE_DOWNLOADER_HPP

#include <filesystem>
#include <string>

class FileDownloader{
    public:
        int downloadFile(void); 

    private:
        std::string const remotePath = "37.221.199.42:~/message";
        std::string const localPath = "/home/pi/";
        std::string const keyFile = "/home/pi/.ssh/id_ed25519";
        std::string const user = "scp-user";
        int const port = 4242;
};

#endif