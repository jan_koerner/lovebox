#ifndef MESSAGE_HPP
#define MESSAGE_HPP
#include <string>
#include <variant>
struct TextMessage{
    std::string content;
};

struct ImageMessage{
    std::string path;
};

struct Message{
    int id;
    std::variant<TextMessage,ImageMessage> message;
};


#endif