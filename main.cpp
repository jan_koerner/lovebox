#include "btn.hpp"
#include "gpioController.hpp"
#include "messageHandler.hpp"
#include "notificationHandler.hpp"

int main(){
    GpioController controller;
    if(controller.isInitialized){
        MessageHandler msgHandler;
        NotificationHandler notHandler;
        Btn btn(2);
        for(;;){
            if(msgHandler.hasNewMessage){
                if(btn.read() == 1){ 
                    notHandler.turnOff();
                    if(msgHandler.hasNewMessage == true){
                        msgHandler.sendToDisplay();
                    }
                }else{ 
                    notHandler.turnOn();
                }
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(200));
        }
    }
}