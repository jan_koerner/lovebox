#ifndef DISPLAY_HPP
#define DISPLAY_HPP
#include <string>
#include <variant>
#include <vector>

class TextMessage;
class ImageMessage;

template <class... Fs> struct Overload : Fs... { using Fs::operator()...; };
template <class... Fs> Overload(Fs...) -> Overload<Fs...>;

class Display{
    public:
        Display(void);
        ~Display(void);
        void displayMessage(std::variant<TextMessage,ImageMessage>& message);
    private:
        int getXPosition(std::string& line);
        int getYStartPosition(int amountLinesNeeded);
        void displayText(std::string& msg);
        void displayImage(std::string path);
        std::vector<std::string> getLines(std::string& msg);
};

#endif