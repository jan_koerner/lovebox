#ifndef BTN_HPP
#define BTN_HPP
#include "notificationHandler.hpp"

class Btn{
    public:
        Btn(unsigned int gpioPort);
        int read(void);
        bool changedState;

    private:
        unsigned int currentLevel;
        unsigned int gpio;
};

#endif