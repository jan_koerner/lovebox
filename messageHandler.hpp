#ifndef MESSAGE_HANDLER_HPP
#define MESSAGE_HANDLER_HPP
#include "display.hpp"
#include "fileDownloader.hpp"
#include "message.hpp"
#include "messageReader.hpp"

#include <atomic>
#include <mutex>
#include <string>
#include <thread>


extern const std::string pathToMsgFile;
class MessageHandler{
    public:
        MessageHandler(void);
        ~MessageHandler(void);
        void sendToDisplay(void);
        bool hasNewMessage;

    private:
        std::thread checkForMessagesThread;
        std::atomic<bool> cancelThread;
        Message currentMsg;
        std::mutex msgMutex;
        void checkForMessages(bool* hasNewMsg);
        bool isDownloadedMesageNew(void);
        int messageHandlerCounter;
        FileDownloader fileDownloader;
        MessageReader messageReader;
        Display display;
        std::string const pathToMsgFile = "/home/pi/message";
    };

#endif