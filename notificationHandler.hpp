#ifndef NOT_HANDLER_HPP
#define NOT_HANDLER_HPP
#include "led.hpp"
#include "motor.hpp"
#include <array>


class NotificationHandler{
    public:
        NotificationHandler(void);
        ~NotificationHandler();
        void turnOn(void);
        void turnOff(void);
        
    private:
        std::array<Led,3> leds;
        Motor motor;
        bool isOn;

};
#endif