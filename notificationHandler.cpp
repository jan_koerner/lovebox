#include "led.hpp"
#include "motor.hpp"
#include "notificationHandler.hpp"
#include <iostream>

NotificationHandler::NotificationHandler():leds{Led(21),Led(20),Led(16)},motor{Motor(18,6,5,25)},isOn{false} {
}

NotificationHandler::~NotificationHandler(){
    turnOff();
}

void NotificationHandler::turnOn(){
    if(isOn == false){
        for(unsigned i=0;i<leds.size();i++){
            leds[i].turnOn();
        }
        motor.start();    
        isOn = true;
    }

}
void NotificationHandler::turnOff(){
    if(isOn == true){
        for(unsigned i=0;i<leds.size();i++){
            leds[i].turnOff();
        }
        motor.stop();    
        isOn = false;
    }
}